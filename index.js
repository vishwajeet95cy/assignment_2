const express = require("express");
const bodyParser = require("body-parser");
const path = require("path");
const mongoose = require("mongoose")
const cors = require("cors");
const dotenv = require("dotenv");
dotenv.config();
const app = express();
const user = require("./routes/user.js");
const authRoute = require('./routes/auth.js');
const postRoute = require('./routes/posts');
const chalk = require('chalk');

const Data = require('./modals/response');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

mongoose.connect(`${process.env.db_host}`, { useUnifiedTopology: true, useNewUrlParser: true }, function (err, db) {
  if (err) throw err;
  console.log(chalk.green('Database Connected'))
});

app.use(cors());

app.use((req, res, next) => {
  let oldSend = res.send;
  res.send = function (data) {
    return new Promise((resolve, reject) => {
      oldSend.apply(res, arguments)
      resolve(data);
    }).then((data) => {
      let response = new Data({
        method: req.method,
        status: res.statusCode,
        url: req.originalUrl,
        body: req.body,
        data: data
      })
      response.save().then((value) => {
        console.log(value);
      }).catch((err) => {
        console.log(err)
      })
    })
  }

  next();
})

app.use('/api', user);
app.use('/auth', authRoute);
app.use('/post', postRoute);

app.get('/', (req, res) => {
  res.send("<h1>Welcome to express js</h1>");
})

app.listen(process.env.PORT, () => {
  console.log(chalk.green(`Example app listening on port ${process.env.PORT}!`))
});