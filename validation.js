// Validation
const Joi = require('@hapi/joi');


// Register Validation
const registerValidation = data => {
  const schema = Joi.object({
    name: Joi.string().min(6).required(),
    email: Joi.string().min(6).required().email(),
    password: Joi.string().min(6).required()
  });
  return schema.validate(data);
};

const loginValidation = data => {
  const schema = Joi.object({
    email: Joi.string().min(6).required().email(),
    password: Joi.string().min(6).required()
  });
  return schema.validate(data);
};

// const formData = data => {
//   const schema = Joi.object({
//     auth: Joi.string().required(),
//     otp: Joi.number().required(),
//     uuid: Joi.string().required(),
//     iemi: Joi.number().required(),
//     phone: Joi.number().required(),
//     userImage: Joi.any().required()
//   });
//   return schema.validate(data);
// }

module.exports.registerValidation = registerValidation;
module.exports.loginValidation = loginValidation;
// module.exports.formData = formData;
