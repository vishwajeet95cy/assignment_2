const express = require("express");
const router = express.Router();
const { get, post, imageFile, vidAdd, videoFile } = require('../Controller/userController')


router.get('/', (req, res) => {
  res.send('Welcome to Home Page');
});

router.get('/get', get);

// multer 
router.post('/upload', imageFile, post);

router.post('/video', videoFile, vidAdd);

module.exports = router;