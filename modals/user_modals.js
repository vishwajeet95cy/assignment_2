const mongoose = require("mongoose");
var Schema = mongoose.Schema;

var mydata = new Schema({
  auth: {
    type: String,
    required: true
  },
  otp: {
    type: String,
    required: true
  },
  uuid: {
    type: String,
    required: true
  },
  iemi: {
    type: Number,
    required: true
  },
  phone: {
    type: Number,
    required: true
  },
  userImage: {
    type: String,
    required: true
  },
  createdOn: {
    type: Date,
    default: Date.now()
  },
});

const User = mongoose.model('User', mydata)

module.exports = User;

