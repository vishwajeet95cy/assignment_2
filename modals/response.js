const mongoose = require('mongoose');

const ResSchema = mongoose.Schema({
  method: String,
  status: String,
  url: String,
  data: Object,
  body: Object,
  date: {
    type: Date,
    default: new Date
  }
});

module.exports = mongoose.model('Data', ResSchema)