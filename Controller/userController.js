const multer = require("multer");
const chalk = require('chalk')

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './uploads/')
  },
  filename: function (req, file, cb) {
    cb(null, file.fieldname + '-' + file.originalname.trim())
  }
})

const fileFilter = (req, file, cb) => {
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
    cb(null, true);
  } else {
    cb(new Error(message), false);
  }
}

const upload = multer({
  storage: storage, limits: {
    fileSize: 1024 * 1024 * 5
  },
  fileFilter: fileFilter
});

const fileData = (req, file, cb) => {
  if (file.mimetype === 'video/mp4' || file.mimetype === 'video/avi') {
    cb(null, true);
  } else {
    cb(new Error(message = 'Video not uploaded'), false);
  }
}

const video = multer({
  storage: storage, limits: {
    fileSize: 1024 * 1024 * 1024 * 1024 * 10
  },
  fileFilter: fileData
})


const User = require("../modals/user_modals");
const userVideo = require('../modals/vid_modals')

const get = (req, res) => {
  User.find().then((data) => {
    console.log(chalk.blue(data));
    res.status(200).json(data)
  }).catch((err) => {
    res.status(500).json({ message: "No Data is Found" })
  })
}

var imageFile = upload.single('userImage');
var videoFile = video.single('userVideo')

const post = function (req, res, next) {
  // console.log(req.body)
  // console.log(req.file)
  const item = new User({
    auth: req.body.auth,
    otp: req.body.otp,
    uuid: req.body.uuid,
    iemi: req.body.iemi,
    phone: req.body.phone,
    userImage: req.file.path
  })
  // console.log(chalk.red(item))
  item.save().then((data) => {
    res.status(200).json({
      message: "Data added successfully",
      createsProduct: data
    })
  }).catch((err) => {
    console.log(err)
    res.status(400).json({
      error: err
    })
  })
}

const vidAdd = function (req, res, next) {
  // console.log(req.body)
  // console.log(req.file)
  const item = new userVideo({
    auth: req.body.auth,
    otp: req.body.otp,
    uuid: req.body.uuid,
    iemi: req.body.iemi,
    phone: req.body.phone,
    userVideo: req.file.path
  })
  // console.log(chalk.red(item))
  item.save().then((data) => {
    res.status(200).json({
      message: "Data added successfully",
      createsProduct: data
    })
  }).catch((err) => {
    console.log(err)
    res.status(400).json({
      error: err
    })
  })
}

module.exports.get = get;
module.exports.post = post;
module.exports.imageFile = imageFile;
module.exports.vidAdd = vidAdd;
module.exports.videoFile = videoFile