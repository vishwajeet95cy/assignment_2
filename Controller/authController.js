const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken')
const UserData = require('../modals/user_auth');

const { registerValidation, loginValidation } = require('../validation');

const register = async (req, res) => {
  // validate data
  const { error } = registerValidation(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  // checking if the user is already in the database
  const emailExist = await UserData.findOne({ email: req.body.email });
  if (emailExist) return res.status(400).send('Email already exist');

  // Hash passwords
  const salt = await bcrypt.genSalt(10);
  const hashedPassword = await bcrypt.hash(req.body.password, salt);

  // create a new user
  const user = new UserData({
    name: req.body.name,
    email: req.body.email,
    password: hashedPassword
  });
  try {
    const savedUser = await user.save();
    res.send({ user: user._id, user: savedUser });
  } catch (err) {
    res.status(400).send(err);
  }
};

const login = async (req, res) => {

  // validate data
  const { error } = loginValidation(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  // checking if the email exists
  const user = await UserData.findOne({ email: req.body.email });
  if (!user) return res.status(400).send('Email is not found');

  // password is correct
  // checking if the user is already in the database
  const validPass = await bcrypt.compare(req.body.password, user.password)
  if (!validPass) return res.status(400).send('Invalid Password');

  //Create and assign a token
  const token = jwt.sign({ _id: user._id }, process.env.Token_secret);
  res.header('auth-token', token).send(token);
};

module.exports.register = register;
module.exports.login = login;